<%--
  Created by IntelliJ IDEA.
  User: siroj
  Date: 9/12/2023
  Time: 2:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <meta charset="UTF-8">
    <title>Sign up</title>
</head>
<body>
<% if (request.getAttribute("errorMsg") != null) {%>
<%
        request.getAttribute("errorMsg");
    }
%>

<h4>Ro'yxatdan o'tish sahifasi</h4>

<form action="/sign-up" method="post">
    <label for="firstName">Ismingizni kiriting</label>
    <input
            type="text"
            id="firstName"
            name="firstName"
    >
    <br>
    <label for="lastName">Familyangiz</label>
    <input
            type="text"
            id="lastName"
            name="lastName"
    >
    <br>
    <label for="email">Emailingiz</label>
    <input
            type="email"
            id="email"
            name="email"
    >
    <br>
    <label for="password">Parolingiz</label>
    <input
            type="password"
            id="password"
            name="password"
    >
    <br>
    <label for="prePassword">Parol takrori</label>
    <input
            type="password"
            id="prePassword"
            name="prePassword"
    >
    <br>
    <button type="submit">Ro'yxatdan o'tish</button>
    <button type="button">Ro'yxatdan o'tish test</button>
    <p>Avval ro'yxatdan o'tganmisiz?</p>
    <a href="/sign-in">Login sahifasiga o'tish</a>
</form>
</body>
