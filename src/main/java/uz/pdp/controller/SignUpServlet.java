package uz.pdp.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.model.ApiResult;
import uz.pdp.model.User;
import uz.pdp.service.DBService;

import java.io.IOException;

public class SignUpServlet extends HttpServlet {
    private final DBService dbService = new DBService();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (dbService.contextniQaytar() == null) {
            dbService.setServletContext(getServletContext());
        }

        boolean signIn = false;
        if (!signIn) {
            resp.sendRedirect("signUp.jsp");
        } else {
            resp.sendRedirect("home.html");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String prePassword = req.getParameter("prePassword");
        ApiResult apiResult = dbService.register(
                User.builder()
                        .email(email)
                        .firstName(firstName)
                        .lastName(lastName)
                        .password(password)
                        .prePassword(prePassword)
                        .build());

        if (apiResult.isSuccess()) {
            resp.sendRedirect("sign-in.jsp");
            return;
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("signUp.jsp");
        req.setAttribute("errorMsg", apiResult.getMsg());
        dispatcher.include(req, resp);

    }
}
