package uz.pdp.service;

import jakarta.servlet.ServletContext;
import uz.pdp.model.ApiResult;
import uz.pdp.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DBService {

    private ServletContext servletContext;

    private static List<User> users =
            Collections.synchronizedList(new ArrayList<>());

    public ApiResult register(User user) {
        if (!user.getPassword().equals(user.getPrePassword()))
            return ApiResult.builder()
                    .success(false)
                    .msg("Parollar mos emas")
                    .build();

        //db qidiramiz email orqali
        Connection connection = connection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT email FROM users WHERE email = ?")) {
            preparedStatement.setString(1, user.getEmail());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return ApiResult.builder()
                        .success(false)
                        .msg("Bunday user allaqachon mavjud")
                        .build();
            }
            PreparedStatement prepareStatement2 = connection.prepareStatement("INSERT INTO users VALUES (DEFAULT,?,?,?,md5(?))");
            prepareStatement2.setString(1, user.getFirstName());
            prepareStatement2.setString(2, user.getLastName());
            prepareStatement2.setString(3, user.getEmail());
            prepareStatement2.setString(4, user.getPassword());
            int executeUpdate = prepareStatement2.executeUpdate();
            System.out.println(executeUpdate);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return ApiResult.builder()
                    .success(false)
                    .msg(sqlException.getMessage())
                    .build();
        }

        return ApiResult.builder()
                .success(true)
                .msg("Muvaffaqiyatli hammasi")
                .build();
    }

    private Connection connection() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(
                    servletContext.getInitParameter("dbUrl"),
                    servletContext.getInitParameter("dbUsername"),
                    servletContext.getInitParameter("dbPassword")
            );
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public ServletContext contextniQaytar() {
        return servletContext;
    }

    public void setServletContext(ServletContext servletContext) {
        if (this.servletContext == null)
            this.servletContext = servletContext;
    }
}
